﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV4
{
    class DiscountedItem : RentableDecorator
    {
        private readonly double discount;

        public DiscountedItem(IRentable rentable, double discount) : base(rentable) 
        {
            this.discount = discount/100;
        }

        public override double CalculatePrice()
        {
            double discountPrice;
            discountPrice = base.CalculatePrice() * this.discount;
            return base.CalculatePrice() - discountPrice;
        }

        public override String Description
        {
            get
            {
                return base.Description +  " now at " + discount * 100 + "% off!";
            }
        }
    }
}
