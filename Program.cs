using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Program.Main12Zad();
            Program.Main345Zad();
        }

        static void Main12Zad() // Main za 1. i 2. zadatak
        {
            Dataset data = new Dataset("LV4CSV.txt");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);

            double[] columnAverage = adapter.CalculateAveragePerColumn(data);
            foreach (double result in columnAverage)
                Console.WriteLine(result);

            Console.WriteLine();
            double[] rowAverage = adapter.CalculateAveragePerRow(data);
            foreach (double result in rowAverage)
                Console.WriteLine(result);
        }

        static void Main345Zad() // Main za 3, 4, i 5. zadatak
        {
            List<IRentable> shoppingCart = new List<IRentable>();
            Video video = new Video("Video1");
            Book book = new Book("Book1");
            shoppingCart.Add(video);
            shoppingCart.Add(book);

            RentingConsolePrinter rent = new RentingConsolePrinter();
            rent.DisplayItems(shoppingCart);
            rent.PrintTotalPrice(shoppingCart); // 3. zadatak do tu, dalje 4.


            Console.WriteLine();
            HotItem hotBook = new HotItem(new Book("HotBook"));
            HotItem hotVideo = new HotItem(new Video("HotVideo"));
            shoppingCart.Add(hotBook);
            shoppingCart.Add(hotVideo);

            rent.DisplayItems(shoppingCart);
            rent.PrintTotalPrice(shoppingCart); // 4. zadatak do tu, dalje 5.


            Console.WriteLine();

            double discount = 25;
            List<IRentable> flashSale = new List<IRentable>();
            DiscountedItem discountedBook = new DiscountedItem(book, discount);
            DiscountedItem discountedVideo = new DiscountedItem(video, discount);
            DiscountedItem discountedHotBook = new DiscountedItem(hotBook, discount);
            DiscountedItem discountedHotVideo = new DiscountedItem(hotVideo, discount);

            flashSale.Add(discountedBook);
            flashSale.Add(discountedVideo);
            flashSale.Add(discountedHotBook);
            flashSale.Add(discountedHotVideo);

            rent.DisplayItems(flashSale);
            rent.PrintTotalPrice(flashSale);
        }
    }
}
